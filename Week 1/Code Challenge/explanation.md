# EXPLANATION

Hello, I will answer the challenges that have been given.

## Geometry

In the `challenge code` folder, there are 3 files, are: 

```bash
greet.js, geometry-cylinder.js and geometry-cube.js.
```
For the `geometry-cylinder.js` file, contains the source code for calculating the volume of a cylinder and the `geometry-cube.js` file contains the source code for calculating the volume of a cube.


