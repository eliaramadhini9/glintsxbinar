// Importing Module
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})


function isEmptyOrSpaces(str) {
	return str === null || str.match(/^ *$/) !== null || str === '0';
} 
// The function cube
function cube(s) {
  return s**3;
}

// The function cylinder
function cylinder(r, t) {
	const phi = 3.14;
	return  phi * (r**2) * t;
}


// Procedure cube
function inputCube() {
	console.clear();
	console.log(`CUBE'S VOLUME`);
	console.log(`=========`);
}

function inputs(){ // fungsi untuk input nilai s
	rl.question("enter a side length (s) value : ",  s => {
		if (!isNaN(s) && !isEmptyOrSpaces(s)) {
			console.log("The volume of a cube is : " + cube(s) );
			quest();
		} else {
			console.log(`Length (s) must be a number\n`);
            inputs()
        }
    })
}


// Procedure cylinder
function inputCylinder() {
	console.clear();
	console.log(`CYLINDER'S VOLUME`);
	console.log(`=========`);
}

function inputRadius(){ // fungsi untuk input nilai radius (r)
	rl.question("enter a radius (r) value : ",  r => {
		if (!isNaN(r) && !isEmptyOrSpaces(r)) {
			inputHeight(r);
		} else {
			console.log(`Radius (r) must be a number\n`);
			inputRadius();
		}
	})
}

function inputHeight(r){ // fungsi untuk input nilai height (t)
	rl.question("enter a height (t) value : ",  t => {
  		if (!isNaN(t) && !isEmptyOrSpaces(t) ) {
  			console.log("The volume of a cylinder is : " + cylinder(r,t) );
			quest();
		} else {
			console.log(`Height (t) must be a number\n`);
			inputHeight(r);
		}
	})
}



function option() {
	rl.question('Answer: ', answer => {
		if (answer == 1) {
			inputCube();
			inputs();
		}
		else if (answer == 2) {
			inputCylinder();
			inputRadius();
		}
		else if (answer == 3){
			console.log("OK, bye!");
			process.exit();
		}
		else if (isNaN(answer)) {
  			console.log("must be a number!");
  			option();
  		}
		else {
			console.log("Sorry, there's no option for that!");
			option();
		}
	})
}

function quest(){
	console.log(`=========`);
	rl.question("do you want to use it again? y/n : ", answers => {
		if (answers == "y") {
			start();
		}
		else if (answers == "n") {
			rl.close();
		}
		else {
			console.log("Sorry, there's no option for that!");
			quest();
		}
	})
}

function start(){
console.clear();
console.log("Which one do you want to calculate?");
console.log(`
1. Volume of a Cube
2. Volume of a Cylinder
3. Exit
`);
option();
}

start();

