const express = require("express"); // Import express
const barangController = require("../controllers/barangController.js");
const barangValidator = require("../middlewares/validators/barangValidator.js");
const router = express.Router(); // Make router from app

const transaksiValidator = require("../middlewares/validators/transaksiValidator.js"); // Import validator to validate every request from user

router.get("/", barangController.getAll); // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get("/:id", barangController.getOne); // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.post("/create", barangValidator.create, barangController.create); // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put("/update/:id", barangValidator.update, barangController.update); // If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.delete("/delete/:id", barangController.delete); // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
