const mongoose = require('mongoose'); // Import mongoose
const mongoose_delete = require('mongoose-delete'); // Import mongoose_delete


const TransaksiScheme = new mongoose.Schema ({
  // Define column
  barang: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  pelanggan: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  jumlah: {
    type: Number,
    required: true
  },
  total: {
    type: mongoose.Schema.Types.Decimal128,
    required: true
  }
}, {
  // timestanmps enable
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false // Disable versioning __v column
})

TransaksiScheme.plugin(mongoose_delete, { overrideMethods: 'all'}) // Enable softdelete
module.exports = transaksi = mongoose.model('transaksi', TransaksiScheme, 'transaksi') // Export transaksi model
