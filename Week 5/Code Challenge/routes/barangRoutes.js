const express = require('express');
const router = express.Router();
const BarangValidator = require('../middlewares/validators/barangValidator');
const BarangController = require('../controllers/barangController');

router.get('/', BarangController.getAll)
router.get('/:id', BarangValidator.getOne, BarangController.getOne)
router.post('/create', BarangValidator.create, BarangController.create)
router.put('/update/:id', BarangValidator.update, BarangController.update)
router.delete('/delete/:id', BarangValidator.delete, BarangController.delete)

module.exports = router
