const express = require('express');
const router = express.Router();
const TransaksiValidator = require('../middlewares/validators/transaksiValidator');
const TransaksiController = require('../controllers/transaksiController');

router.get('/', TransaksiController.getAll)
router.get('/:id', TransaksiValidator.getOne, TransaksiController.getOne)
router.post('/create', TransaksiValidator.create, TransaksiController.create)
router.put('/update/:id', TransaksiValidator.update, TransaksiController.update)
router.delete('/delete/:id', TransaksiValidator.delete,TransaksiController.delete)

module.exports = router
