const express = require('express');
const router = express.Router();
const PelangganValidator = require('../middlewares/validators/pelangganValidator');
const PelangganController = require('../controllers/pelangganController');

router.get('/', PelangganController.getAll)
router.get('/:id', PelangganValidator.getOne, PelangganController.getOne)
router.post('/create', PelangganValidator.create, PelangganController.create)
router.put('/update/:id', PelangganValidator.update, PelangganController.update)
router.delete('/delete/:id', PelangganValidator.delete, PelangganController.delete)

module.exports = router
