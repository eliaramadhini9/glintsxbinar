const express = require('express');
const router = express.Router();
const PemasokValidator = require('../middlewares/validators/pemasokValidator');
const PemasokController = require('../controllers/pemasokController');

router.get('/', PemasokController.getAll)
router.get('/:id', PemasokValidator.getOne, PemasokController.getOne)
router.post('/create', PemasokValidator.create, PemasokController.create)
router.put('/update/:id', PemasokValidator.update, PemasokController.update)
router.delete('/delete/:id', PemasokValidator.delete, PemasokController.delete)

module.exports = router
