const { pemasok } = require('../models')

class PemasokController {

  async getAll(req, res) {
    pemasok.find({}).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getOne(req, res) {
    pemasok.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {
    pemasok.create({
      nama: req.body.nama
    }).then(result => {
      res.json({
        status: "Data baru berhasil ditambahkan ke tabel pemasok",
        data: result
      })
    })
  }

  async update(req, res) {
    pemasok.findOneAndUpdate({
      _id: req.params.id
    }, {
      nama: req.body.nama
    }).then(() => {
      return pemasok.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "Data tabel pemasok berhasil diperbarui",
        data: result
      })
    })
  }

  async delete(req, res) {
    pemasok.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "Data berhasil dihapus",
        data: null
      })
    })
  }

}

module.exports = new PemasokController
