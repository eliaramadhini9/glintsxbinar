const { barang, pemasok } = require('../models')

class BarangController {

  async getAll(req, res) {
    barang.find({}).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getOne(req, res) {
    barang.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      })
    ])

    barang.create({
      nama: req.body.nama,
      harga: req.body.harga,
      pemasok: data[0],
      image:req.file===undefined?"":req.file.filename
    }).then(result => {
      res.json({
        status: "Data baru berhasil ditambahkan ke tabel barang",
        data: result
      })
    })
  }

  async update(req, res) {
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      })
    ])

    barang.findOneAndUpdate({
      _id: req.params.id
    }, {
      nama: req.body.nama,
      harga: req.body.harga,
      pemasok: data[0],
      image: req.file===undefined?"":req.file.filename
    }).then(() => {
      return barang.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "Data tabel barang berhasil diperbarui",
        data: result
      })
    })
  }

  async delete(req, res) {
    barang.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "data berhasil dihapus",
        data: null
      })
    })
  }

}

module.exports = new BarangController
