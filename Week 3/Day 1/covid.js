const ex = require("./login.js");
const rl = ex.rl;

function isEmptyOrSpaces(str) {
	return str === null || str.match(/^ *$/) !== null || str === '0';
} 

// data array
let data = [
{
	name: "John",
	location: "Jawa Barat",
    status: "Positive"
},
{
	name: "Johny",
	location: "Jakarta",
    status: "Suspect"
},
{
    name: "Mike",
    location: "Jawa Tengah",
    status: "Suspect"
},
{
    name: "lola",
    location: "Jawa Tengah",
    status: "Suspect"
},
{
	name: "Ken",
	location: "Jawa Timur",
    status: "Positive"
},
{
	name: "Kenzo",
	location: "Jawa Barat",
    status: "Suspect"
},
{
    name: "Lily",
    location: "Jakarta",
    status: "Positive"
},
{
    name: "Jinny",
    location: "Jawa Tengah",
    status: "Positive"
}
]

// choose the options
function start(){
	console.clear()
    console.log("CORONA CASE DATA");
    console.log("================");
    console.log("You can choose based on the options below: ");
    console.log("1. Positive");
    console.log("2. Suspect");
    console.log("3. Exit");
    ex.rl.question("Answer : ", choose=>{
        switch(Number(choose)) {
            case 1:
                positive()
                break
            case 2:
                suspect()
                break
            case 3:
                exit()
                break
            default:
                console.log(`there's no option for ${choose}`)
                start()
                break
        }
    })
}

// Positive
function positive() {
	console.log(`POSITIVE CASE :`)
	let a = 0
	for (let i = 0; i < data.length; i++) {
		if (data[i].status === "Positive") {
			a++
			console.log(`${a}. ${data[i].name} - (Location: ${data[i].location})`)
		} 
	}
	quest()
}

// Suspect
function suspect() {
	let a = 0
	for (let i = 0; i < data.length; i++) {
		if (data[i].status === "Suspect") {
			a++
			console.log(`${a}. ${data[i].name} - (Location: ${data[i].location})`)
		} 
	}
	quest()
}

// Close the program
function exit() {
	ex.rl.question(`are you sure? y/n : `, option => {
		if (option == `y`) {
			process.exit()
		} else if (option == `n`) {
			start()
		} else {
			console.log(`just choose y or n`)
			exit()
		}
	})
}

function quest() {
	console.log(`=========`);
	ex.rl.question("want to see the data again?? y/n : ", (answers) => {
		if (answers == "y") {
			start()
		} else if (answers == "n") {
			ex.rl.close()
		} else {
			quest()
		}
	});
}


module.exports = start


