const EvenEmitter = require(`events`)
const emit = new EvenEmitter()
const readline = require('readline')
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
})

const covid = require('./covid.js')

function paternEmail (str) {
	return str.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/)
}

const wrongEmail = (email) => {
	console.log(`\nemail format is wrong!! (ex: ${email}@gmail.com)`)
	rl.close()
}

const loginFailed = (email) => {
	console.log(`\nwrong password! ${email} failed to login`)
	rl.close()
}

const loginSuccess = (email) => {
	covid()
}

emit.on('Wrong Email', wrongEmail)
emit.on('Login Failed', loginFailed)
emit.on('Login Success', loginSuccess)

const user = {
	login(email, password) {
		const pass = '12345'

		if (!paternEmail(email)) {
			emit.emit('Wrong Email', email)
		} else if (password != pass) {
			emit.emit('Login Failed', email)
		} else {
			emit.emit('Login Success', email)
		}
	}
}

let i="*"
rl._writeToOutput=function _writeToOutput(sandinya) {
  if (rl.stdoutMuted) {
    i+="*"
    rl.input.write("\x1b[2K\x1b[200D"+rl.query+i.slice(2,rl.line.length+2))
  } else {
    rl.output.write(sandinya)
  }
}

rl.question(`Email : `, email => {
    rl.stdoutMuted=true
    rl.query="Password : "
    rl.question(`Password : `,password => {
      rl.stdoutMuted=false
      user.login(email, password)
    })
  })



module.exports.rl = rl
