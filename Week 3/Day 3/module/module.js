const tabung = require('./tabung.js')


class Module {
	constructor() {
		this.name = 'Menghitung Bangun Ruang'
	}

	menghitungKelilingTabung(radius) {
		let hitungKelilingTabung = new tabung(radius)
		return hitungKelilingTabung.menghitungKeliling()
	}

	menghitungLuasTabung(radius) {
		let hitungLuasTabung = new tabung(radius)
		return hitungLuasTabung.menghitungLuas()
	}

	menghitungVolumeTabung(radius, tinggi) {
		let hitungVolumeTabung = new tabung(radius, tinggi)
		return hitungVolumeTabung.menghitungVolume()
	}
}

module.exports = Module