class BangunRuang {
	constructor(name) {
		if (this.constructor === BangunRuang) {
			throw new Error(`This is abstract!`)
		}
		this.name = name
	}


	menghitungKeliling() {
		console.log(`Keliling Tabung`)
	}

	menghitungLuas() {
		console.log(`Luas Tabung`)
	}

	menghitungVolume() {
		console.log(`Volume Tabung`)
	}
}

module.exports = BangunRuang
