const importBangunRuang = require('./bangunRuang.js')

class Tabung extends importBangunRuang {

	constructor(radius, tinggi) {
		super('Tabung')
		this.radius = radius
		this.tinggi = tinggi
	}

	menghitungKeliling() {
		super.menghitungKeliling()
		return 2 * Math.PI * this.radius
	}

	menghitungLuas() {
		super.menghitungLuas()
		return 2 * Math.PI * Math.pow(this.radius, 2)
	}

	menghitungVolume() {
		super.menghitungVolume()
		return Math.PI * Math.pow(this.radius, 2) * this.tinggi
	}
}

module.exports = Tabung