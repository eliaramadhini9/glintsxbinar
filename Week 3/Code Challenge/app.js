// Import expressjs to make API
const express = require('express') // Import expressjs
const app = express() // make instance object of expressjs
const indexRoutes = require('./routes/indexRoutes.js') // import hello Routes that will be used if we accessing localhost:3000/*

app.use(express.static('public')); // make static file like images, videos, css or others file in public directory

app.use('/', indexRoutes)
app.use('/elia', indexRoutes)

app.listen(3000) // will have port number 3000
